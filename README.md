# Navigator Maps

[![This image on DockerHub](https://img.shields.io/docker/pulls/stuartshay/navigator-maps.svg)](https://hub.docker.com/r/stuartshay/navigator-maps/) [![CircleCI](https://circleci.com/gh/stuartshay/NavigatorMaps.svg?style=svg)](https://circleci.com/gh/stuartshay/NavigatorMaps)


#### Demo
https://maps.navigatorglass.com     
http://navigatormaps.herokuapp.com/  

### Prerequisites

````
git
nodejs > 0.10.x
npm > 1.4.x
heroku toolbelt ~latest
````

````
npm install -g bower grunt-cli
````

### Setup

##### Bind All IP
````
export IP=0.0.0.0
````

##### Set Node Environment (Dev/Stage/Prod)

###### Linux
````
export NODE_ENV=Stage
export PORT="3000"
export API_PORT="7000"
````

###### Windows 
````
set NODE_ENV=Stage
set PORT=3000
set API_PORT=7000

echo NODE_ENV:%NODE_ENV% Port:%PORT% API_PORT:%API_PORT%
````

##### Install & Run
````
cd NavigatorMaps
npm install
grunt serve
````

#### Heroku Deployment

````
cd NavigatorMaps
heroku create
git push heroku master
````

#### Build using Docker

##### Local

```
docker build -t navigator-maps .
docker run -d -p 3000:3000 navigator-maps
```

 `http://DOCKER_IP:3000`

##### Docker Hub

```
docker pull stuartshay/navigator-maps:stable
docker run -p 80:3000 stuartshay/navigator-maps:stable
```

##### Tag & Push Stable Image

````
docker tag <imageid> navigator-maps:stable
docker tag navigator-maps:stable  stuartshay/navigator-maps:stable

docker push stuartshay/navigator-maps:stable
````
