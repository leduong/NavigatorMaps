FROM node:6.10.2

# Set environment variables
ENV PORT="3000"
ENV NODE_ENV="Stage"
ENV API_PORT="5000"

# Create and copy app directory
RUN mkdir -p /app
WORKDIR /app
COPY ./dist /app/dist
COPY _package.json /app/package.json
COPY server.js /app/

# Install app dependencies
RUN npm i

EXPOSE 3000
CMD [ "npm", "start" ]
