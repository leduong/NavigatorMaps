'use strict';
angular.module('navigatorMaps').

controller('MapCtrl', [
  '$scope', '$rootScope', '$stateParams', '$timeout', 'Map', 'MapUtil', 'FeatureService', 'MAP_SETTINGS',
  function($scope, $rootScope, $stateParams, $timeout, Map, MapUtil, FeatureService, MAP_SETTINGS) {
    var
      i = 0,
      markerUrl = 'img/markers/',
      ret = [],
      arrPolys = [],
      features = Map.features || [];
    $scope.map = {
      bounds: {}
    };

    function marker(color, label) {
      return markerUrl + color + '/marker' + label + '.png';
    }

    function select(photo) {
      $scope.coords = {
        latitude: photo.latitude,
        longitude: photo.longitude
      };
      $scope.selected = photo;
      $scope.icon = marker('red', photo.id);
    }

    function scrollToListItem(id) {
      var divList, itemHeight, y;
      divList = angular.element('.photos-list')[0];
      itemHeight = divList.scrollHeight / features.length;
      y = itemHeight * (id - 1);
      divList.scrollTop = y;
    }

    angular.forEach(features, function(item) {
      i++;
      var url_m, url_t;
      var photos = item.properties.photoSizes;

      photos.filter(function(photo) {
        if (photo.prefix === 's') {
          url_t = photo.url;
        } else if (photo.prefix === 'm') {
          url_m = photo.url;
        }
      });

      this.push({
        id: i,
        longitude: item.geometry.coordinates[0],
        latitude: item.geometry.coordinates[1],
        title: item.properties.title,
        objectType: item.properties.objectType,
        url: url_t,
        url_m: url_m,
        icon: marker('green', i)
      });
    }, ret);

    $scope.maps = ret;

    $scope.events = {
      'click': function(markr, ev, model) {
        select(model);
        scrollToListItem(model.id);
      }
    };

    if (ret.length > 0) {
      $scope.map = {
        center: {
          latitude: ret[0].latitude,
          longitude: ret[0].longitude
        },
        zoom: 15
      };

      select(ret[0]);
    }

    var bounds = MapUtil.getBounds(ret);

    $timeout(function() {
      $scope.map.bounds = {
        northeast: {
          latitude: bounds.getNorthEast().lat(),
          longitude: bounds.getNorthEast().lng()
        },
        southwest: {
          latitude: bounds.getSouthWest().lat(),
          longitude: bounds.getSouthWest().lng()
        }
      };

    }, 100);

    $scope.opts = {
      zIndex: 10000000
    };

    $scope.options = {
      scrollwheel: true,
      key: MAP_SETTINGS.key,
      maxZoom: MAP_SETTINGS.maxZoom
    };

    $scope.mapInfo = {};

    $scope.showInfo = function(obj) {
      $scope.mapInfo = obj;
    };

    $scope.selectPhoto = function(photo) {
      $scope.map.center = {
        latitude: photo.latitude,
        longitude: photo.longitude
      };
      $scope.show = true;
      select(photo);
    };

    $scope.img_load = function() {
      $scope.show = false;
    };

    $scope.$on('feature', function(event, id) {
      if (id) {
        FeatureService.features(id, function(polys) {
          $timeout(function() {
            if ($rootScope.app.overlay.show === 'true') {
              $scope.polys = polys;
            } else {
              $scope.polys = [];
            }
            arrPolys = polys;
          }, 0);
        });
      }
    });

    $scope.$on('overlay.show', function(event, show) {
      if (show === 'false') {
        $scope.polys = [];
      } else {
        $scope.polys = arrPolys;
      }
    });

    $scope.polys = [];
    $rootScope.$broadcast('app.mapId', $stateParams.Id);
  }
]);
