'use strict';
angular.module('navigatorMaps').

factory('FeatureService', [
	'$http', 'API', 'filterFilter', 'MapUtil',
	function ($http, API, filterFilter, MapUtil) {
		var exports;

		function processFeatures (data) {
			var features = data.features || [];
			var i = 0, center;
			var polys = filterFilter(features, {
				geometry: {
					type: 'Polygon, LineString'
				}
			}, function (expected, actual) {
				return actual.indexOf(expected) > -1;
			});

			angular.forEach(polys, function (poly) {
				poly.id = ++i;

				center = MapUtil.findCenter(poly.geometry.coordinates);

				poly.latitude = center.lat();
				poly.longitude = center.lng();

				poly.options = {
					labelContent: i,
					labelAnchor: (i < 100) ? '4 8' : '10 8',
					labelClass: 'marker-labels',
					opacity: 0.01
				};
			});
			return polys;
		}

		exports = {
			'default': {
				id: ''
			},
			query: function () {
				return $http.get(API.apiUrl + 'features');
			},
			create: function (params) {
				return $http.post(API.apiUrl + 'features', params);
			},
			get: function (id) {
				return $http.get(API.apiUrl + 'features/' + id);
			},
			update: function (id, params) {
				return $http.put(API.apiUrl + 'features/' + id, params);
			},
			features: function (id, cb) {
				exports.get(id).then(function(res) {
					var data = res.data || {};
					var polys = processFeatures (data);
					cb(polys);
				});
			}
		};
		return exports;
	}
]);
