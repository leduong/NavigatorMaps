'use strict';
angular.module('navigatorMaps').

controller('FeatureCtrl', [
	'$scope', 'Feature', 'MapUtil', 'filterFilter',
	function ($scope, Feature, MapUtil, filterFilter) {
		var features = (Feature && typeof Feature.features !== 'undefined') ? Feature.features : [];
		var polys = [],
			center,
			i = 0;

		$scope.map = {
			pan: true,
			refresh: false,
			events: {},
			bounds: {},
			polys: [],
			getPolyFill: function () {
				return {
					color: '#2c8aa7',
					opacity: '0.3'
				};
			},
			polyEvents: {
				click: function (gPoly, eventName, polyModel) {
					console.log('Poly Clicked: id:' + polyModel.$id + ' ' + JSON.stringify(polyModel.path));
				}
			},
			draw: undefined
		};

		polys = filterFilter(features, {
			geometry: {
				type: 'Polygon, LineString'
			}
		}, function (expected, actual) {
			return actual.indexOf(expected) > -1;
		});

		angular.forEach(polys, function (poly) {
			poly.id = ++i;

			center = MapUtil.findCenter(poly.geometry.coordinates);

			poly.latitude = center.lat();
			poly.longitude = center.lng();

			poly.options = {
				labelContent: i,
				labelAnchor: (i < 100) ? '4 8' : '10 8',
				labelClass: 'marker-labels',
				opacity: 0.01
			};
		});

		if (polys.length > 0) {
			$scope.map = {
				center: {
					latitude: polys[0].latitude,
					longitude: polys[0].longitude
				},
				zoom: 15
			};
		}

		$scope.map.polys = polys;

		$scope.options = {
			scrollwheel: false
		};

		$scope.mapInfo = {};

		$scope.showInfo = function (obj) {
			$scope.mapInfo = obj;
		};

	}
]);