'use strict';
angular.module('navigatorMaps').

factory('MapUtil', [
	function () {
		var exports;

		exports = {
			'findCenter': function (path) {
				var coords = [];
				var bounds = new google.maps.LatLngBounds();

				path = (Array.isArray(path[0][0])) ? path[0] : path;

				path.forEach(function (co) {
					coords.push(new google.maps.LatLng(co[1], co[0]));
				});

				for (var i = 0; i < coords.length; i++) {
					bounds.extend(coords[i]);
				}
				return bounds.getCenter();
			},

			'getBounds': function (coords) {
				var bounds = new google.maps.LatLngBounds();

				for (var i = 0; i < coords.length; i++) {
					bounds.extend(new google.maps.LatLng(coords[i].latitude, coords[i].longitude));
				}

				return bounds;
			}
		};

		return exports;
	}
]);